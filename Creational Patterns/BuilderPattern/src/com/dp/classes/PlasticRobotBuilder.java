package com.dp.classes;

import com.dp.interfaces.IRobotBuilder;


public class PlasticRobotBuilder implements IRobotBuilder {

	private Robot robot;
	
	public PlasticRobotBuilder(){
		robot = new Robot();
	}
	
	public void buildRobotHead() {
		
		robot.setRobotHead("Plastic robot head");
	}

	public void buildRobotTorso() {
		robot.setRobotTorso("Plastic torso");
		
	}

	public void buildRobotArms() {
		robot.setRobotArms("Blowtorch Arms");
		
	}

	public void buildRobotLegs() {
		robot.setRobotLegs("Rollar Skates");
		
	}

	public void buildPowerSource() {
		robot.setPowerSource("Batteries");
		
	}
	
	public Robot getRobot() {
		return this.robot;
	}
}