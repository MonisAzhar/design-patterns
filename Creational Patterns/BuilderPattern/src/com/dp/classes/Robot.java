package com.dp.classes;

import com.dp.interfaces.IRobotPlan;

public class Robot implements IRobotPlan {

	private String robotHead;
	private String robotTorso;
	private String robotArms;
	private String robotLegs;
	private String powerSource;

	public void setRobotHead(String head) {

		robotHead = head;

	}

	public String getRobotHead() {
		return robotHead;
	}

	public void setRobotTorso(String torso) {

		robotTorso = torso;

	}

	public String getRobotTorso() {
		return robotTorso;
	}

	public void setRobotArms(String arms) {

		robotArms = arms;

	}

	public String getRobotArms() {
		return robotArms;
	}

	public void setRobotLegs(String legs) {

		robotLegs = legs;

	}

	public String getRobotLegs() {
		return robotLegs;
	}

	public void setPowerSource(String source) {
		powerSource = source;
	}

	public String getPowerSource() {
		return powerSource;
	}
}
