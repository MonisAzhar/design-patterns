package com.dp.classes;

import com.dp.interfaces.IRobotBuilder;

public class OldRobotBuilder implements IRobotBuilder {

	private Robot robot;

	public OldRobotBuilder() {

		this.robot = new Robot();

	}

	public void buildRobotHead() {

		robot.setRobotHead("Tin Head");

	}

	public void buildRobotTorso() {

		robot.setRobotTorso("Tin Torso");

	}

	public void buildRobotArms() {

		robot.setRobotArms("Blowtorch Arms");

	}

	public void buildRobotLegs() {

		robot.setRobotLegs("Rollar Skates");

	}

	public Robot getRobot() {

		return this.robot;
	}

	
	public void buildPowerSource() {
		robot.setPowerSource("AC Voltage");
	}
}