package com.dp.main;

import com.dp.classes.OldRobotBuilder;
import com.dp.classes.PlasticRobotBuilder;
import com.dp.classes.Robot;
import com.dp.classes.RobotEngineer;
import com.dp.interfaces.IRobotBuilder;


public class Main {

    public static void main(String[] args) {
    	
    	builderDemo();
    }

    private static void builderDemo(){
        IRobotBuilder plasticRobot = new PlasticRobotBuilder();
    	
        IRobotBuilder oldStyleRobot = new OldRobotBuilder();

        RobotEngineer robotEngineer = new RobotEngineer(oldStyleRobot);
        robotEngineer.makeRobot();

        
        Robot firstRobot = robotEngineer.getRobot();
        System.out.println("Old Robot Built");

        System.out.println("Robot Head Type: " + firstRobot.getRobotHead());

        System.out.println("Robot Torso Type: " + firstRobot.getRobotTorso());

        System.out.println("Robot Arm Type: " + firstRobot.getRobotArms());

        System.out.println("Robot Leg Type: " + firstRobot.getRobotLegs());
        
        System.out.println("Robot Power Source Type: " + firstRobot.getPowerSource());

        
        
        
        robotEngineer = new RobotEngineer(plasticRobot);
        
        robotEngineer.makeRobot();
        Robot secondRobot = robotEngineer.getRobot();
             
        
        
        
        System.out.println("Plastic Robot Built");

        System.out.println("Robot Head Type: " + secondRobot.getRobotHead());

        System.out.println("Robot Torso Type: " + secondRobot.getRobotTorso());

        System.out.println("Robot Arm Type: " + secondRobot.getRobotArms());

        System.out.println("Robot Leg Type: " + secondRobot.getRobotLegs());
        
        System.out.println("Robot Power Source Type: " + secondRobot.getPowerSource());

    }
}