package com.dp.interfaces;

import com.dp.classes.Robot;

public interface IRobotBuilder {

    public void buildRobotHead();

    public void buildRobotTorso();

    public void buildRobotArms();

    public void buildRobotLegs();
    
    public void buildPowerSource();

    public Robot getRobot();
}