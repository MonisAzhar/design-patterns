package com.dp.interfaces;

public interface IRobotPlan{

    public void setRobotHead(String head);

    public void setRobotTorso(String torso);

    public void setRobotArms(String arms);

    public void setRobotLegs(String legs);
    
    public void setPowerSource(String source);
}