package com.dp.interaces;

import com.dp.classes.Student;

public interface IClonable {
	public Student clone();
}