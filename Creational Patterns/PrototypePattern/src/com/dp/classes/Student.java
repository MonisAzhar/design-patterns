package com.dp.classes;

import com.dp.interaces.IClonable;

public class Student implements IClonable {

	String name, email;
	int age, studentId;

	
	public Student(String name, String email, int age, int studentId) {
		this.name = name;
		this.email = email;
		this.age = age;
		this.studentId = studentId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	public void setStudentId(int id){
		this.studentId = id;
	}
	public int getStudentId(){
		return studentId;
	}
	@Override
	public Student clone()
	{
		return new Student(getName(), getEmail(),getAge(), getStudentId());
	}
}