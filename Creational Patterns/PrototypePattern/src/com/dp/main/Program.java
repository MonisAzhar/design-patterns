package com.dp.main;

import com.dp.classes.Student;

public class Program {
	public static void main(String[] args) {
		Student s1 = new Student("Monis","monis_azhar@hotmail.com",22, 59485);
		
		Student s2 = s1.clone();
		
		System.out.println(s2);
		System.out.println(s1);
	}
}